diff --git a/make/autoconf/hotspot.m4 b/make/autoconf/hotspot.m4
index d93e23ce9..f59254374 100644
--- a/make/autoconf/hotspot.m4
+++ b/make/autoconf/hotspot.m4
@@ -420,10 +420,11 @@ AC_DEFUN_ONCE([HOTSPOT_SETUP_JVM_FEATURES],
     JVM_FEATURES_jvmci=""
     INCLUDE_JVMCI="false"
   else
-    # Only enable jvmci on x86_64, sparcv9 and aarch64
+    # Only enable jvmci on x86_64, sparcv9, aarch64 and riscv64
     if test "x$OPENJDK_TARGET_CPU" = "xx86_64" || \
        test "x$OPENJDK_TARGET_CPU" = "xsparcv9" || \
-       test "x$OPENJDK_TARGET_CPU" = "xaarch64" ; then
+       test "x$OPENJDK_TARGET_CPU" = "xaarch64" || \
+       test "x$OPENJDK_TARGET_CPU" = "xriscv64"; then
       AC_MSG_RESULT([yes])
       JVM_FEATURES_jvmci="jvmci"
       INCLUDE_JVMCI="true"
diff --git a/src/hotspot/cpu/riscv64/interp_masm_riscv64.cpp b/src/hotspot/cpu/riscv64/interp_masm_riscv64.cpp
index e3d1b716c..6d6f8aacc 100644
--- a/src/hotspot/cpu/riscv64/interp_masm_riscv64.cpp
+++ b/src/hotspot/cpu/riscv64/interp_masm_riscv64.cpp
@@ -1240,12 +1240,21 @@ void InterpreterMacroAssembler::record_klass_in_profile_helper(
     if (is_virtual_call) {
       increment_mdp_data_at(mdp, in_bytes(CounterData::count_offset()));
     }
-
+#if INCLUDE_JVMCI
+    else if (EnableJVMCI) {
+      increment_mdp_data_at(mdp, in_bytes(ReceiverTypeData::nonprofiled_receiver_count_offset()));
+    }
+#endif // INCLUDE_JVMCI
   } else {
     int non_profiled_offset = -1;
     if (is_virtual_call) {
       non_profiled_offset = in_bytes(CounterData::count_offset());
     }
+#if INCLUDE_JVMCI
+    else if (EnableJVMCI) {
+      non_profiled_offset = in_bytes(ReceiverTypeData::nonprofiled_receiver_count_offset());
+    }
+#endif // INCLUDE_JVMCI
 
     record_item_in_profile_helper(receiver, mdp, reg2, 0, done, TypeProfileWidth,
       &VirtualCallData::receiver_offset, &VirtualCallData::receiver_count_offset, non_profiled_offset);
diff --git a/src/hotspot/cpu/riscv64/riscv64.ad b/src/hotspot/cpu/riscv64/riscv64.ad
index b9cd8402a..109ad6577 100644
--- a/src/hotspot/cpu/riscv64/riscv64.ad
+++ b/src/hotspot/cpu/riscv64/riscv64.ad
@@ -1465,7 +1465,7 @@ const bool Matcher::match_rule_supported(int opcode) {
 
 // Identify extra cases that we might want to provide match rules for vector nodes and
 // other intrinsics guarded with vector length (vlen) and element type (bt).
-const bool Matcher::match_rule_supported_vector(int opcode, int vlen) {
+const bool Matcher::match_rule_supported_vector(int opcode, int vlen, BasicType bt) {
   return false;
 }
 
@@ -1530,6 +1530,14 @@ const int Matcher::min_vector_size(const BasicType bt) {
   return 0;
 }
 
+const bool Matcher::supports_scalable_vector() {
+  return 0;
+}
+
+const int Matcher::scalable_vector_reg_size(const BasicType bt) {
+  return Matcher::max_vector_size(bt);
+}
+
 // Vector ideal reg.
 const uint Matcher::vector_ideal_reg(int len) {
   return 0;
diff --git a/src/hotspot/cpu/riscv64/sharedRuntime_riscv64.cpp b/src/hotspot/cpu/riscv64/sharedRuntime_riscv64.cpp
index 1b3202efc..7266d9973 100644
--- a/src/hotspot/cpu/riscv64/sharedRuntime_riscv64.cpp
+++ b/src/hotspot/cpu/riscv64/sharedRuntime_riscv64.cpp
@@ -47,6 +47,9 @@
 #include "adfiles/ad_riscv64.hpp"
 #include "opto/runtime.hpp"
 #endif
+#if INCLUDE_JVMCI
+#include "jvmci/jvmciJavaClasses.hpp"
+#endif
 
 #define __ masm->
 
@@ -471,6 +474,18 @@ void SharedRuntime::gen_i2c_adapter(MacroAssembler *masm,
   // Pre-load the register-jump target early, to schedule it better.
   __ ld(t1, Address(xmethod, in_bytes(Method::from_compiled_offset())));
 
+#if INCLUDE_JVMCI
+  if (EnableJVMCI || UseAOT) {
+    // check if this call should be routed towards a specific entry point
+    __ ld(t1, Address(xthread, in_bytes(JavaThread::jvmci_alternate_call_target_offset())));
+    Label no_alternative_target;
+    __ beqz(t1, no_alternative_target);
+    __ mv(t0, t1);
+    __ sd(zr, Address(xthread, in_bytes(JavaThread::jvmci_alternate_call_target_offset())));
+    __ bind(no_alternative_target);
+  }
+#endif // INCLUDE_JVMCI
+
   // Now generate the shuffle code.
   for (int i = 0; i < total_args_passed; i++) {
     if (sig_bt[i] == T_VOID) {
@@ -2040,6 +2055,11 @@ void SharedRuntime::generate_deopt_blob() {
   ResourceMark rm;
   // Setup code generation tools
   int pad = 0;
+#if INCLUDE_JVMCI
+  if (EnableJVMCI || UseAOT) {
+    pad += 512; // Increase the buffer size when compiling for JVMCI
+  }
+#endif
   CodeBuffer buffer("deopt_blob", 2048 + pad, 1024);
   MacroAssembler* masm = new MacroAssembler(&buffer);
   int frame_size_in_words = -1;
@@ -2091,6 +2111,12 @@ void SharedRuntime::generate_deopt_blob() {
   __ j(cont);
 
   int reexecute_offset = __ pc() - start;
+#if INCLUDE_JVMCI && !defined(COMPILER1)
+  if (EnableJVMCI && UseJVMCICompiler) {
+    // JVMCI does not use this kind of deoptimization
+    __ should_not_reach_here();
+  }
+#endif
 
   // Reexecute case
   // return address is the pc describes what bci to do re-execute at
@@ -2101,6 +2127,45 @@ void SharedRuntime::generate_deopt_blob() {
   __ mvw(xcpool, Deoptimization::Unpack_reexecute); // callee-saved
   __ j(cont);
 
+#if INCLUDE_JVMCI
+  Label after_fetch_unroll_info_call;
+  int implicit_exception_uncommon_trap_offset = 0;
+  int uncommon_trap_offset = 0;
+
+  if (EnableJVMCI || UseAOT) {
+    implicit_exception_uncommon_trap_offset = __ pc() - start;
+
+    __ ld(lr, Address(xthread, in_bytes(JavaThread::jvmci_implicit_exception_pc_offset())));
+    __ ld(zr, Address(xthread, in_bytes(JavaThread::jvmci_implicit_exception_pc_offset())));
+
+    uncommon_trap_offset = __ pc() - start;
+
+    // Save everything in sight.
+    reg_saver.save_live_registers(masm, 0, &frame_size_in_words);
+    // fetch_unroll_info nees to call last_java_frame()
+    Label retaddr;
+    __ set_last_Java_frame(sp, noreg, retaddr, t0);
+
+    __ lwu(c_rarg1, Address(xthread, in_bytes(JavaThread::pending_deoptimization_offset())));
+    __ mvw(t0, -1);
+    __ sw(t0, Address(xthread, in_bytes(JavaThread::pending_deoptimization_offset())));
+
+    __ mvw(xcpool, (int32_t)Deoptimization::Unpack_reexecute);
+    __ mv(c_rarg0, xthread);
+    __ mv(c_rarg2, xcpool); // exec mode
+    __ la(t0,
+          RuntimeAddress(CAST_FROM_FN_PTR(address,
+                                          Deoptimization::uncommon_trap)));
+    __ jalr(x1, t0, 0);
+    __ bind(retaddr);
+    oop_maps->add_gc_map( __ pc()-start, map->deep_copy());
+
+    __ reset_last_Java_frame(false);
+
+    __ j(after_fetch_unroll_info_call);
+  } // EnableJVMCI
+#endif // INCLUDE_JVMCI
+
   int exception_offset = __ pc() - start;
 
   // Prolog for exception case
@@ -2194,6 +2259,12 @@ void SharedRuntime::generate_deopt_blob() {
 
   __ reset_last_Java_frame(false);
 
+#if INCLUDE_JVMCI
+  if (EnableJVMCI || UseAOT) {
+    __ bind(after_fetch_unroll_info_call);
+  }
+#endif
+
   // Load UnrollBlock* into x15
   __ mv(x15, x10);
 
@@ -2349,8 +2420,14 @@ void SharedRuntime::generate_deopt_blob() {
   masm->flush();
 
   _deopt_blob = DeoptimizationBlob::create(&buffer, oop_maps, 0, exception_offset, reexecute_offset, frame_size_in_words);
-  assert(_deopt_blob != NULL, "create deoptimization blob fail!");
+  //assert(_deopt_blob != NULL, "create deoptimization blob fail!");
   _deopt_blob->set_unpack_with_exception_in_tls_offset(exception_in_tls_offset);
+#if INCLUDE_JVMCI
+  if (EnableJVMCI || UseAOT) {
+    _deopt_blob->set_uncommon_trap_offset(uncommon_trap_offset);
+    _deopt_blob->set_implicit_exception_uncommon_trap_offset(implicit_exception_uncommon_trap_offset);
+  }
+#endif
 }
 
 uint SharedRuntime::out_preserve_stack_slots() {
diff --git a/src/hotspot/cpu/riscv64/templateInterpreterGenerator_riscv64.cpp b/src/hotspot/cpu/riscv64/templateInterpreterGenerator_riscv64.cpp
index 83b64184d..4d6f08b26 100644
--- a/src/hotspot/cpu/riscv64/templateInterpreterGenerator_riscv64.cpp
+++ b/src/hotspot/cpu/riscv64/templateInterpreterGenerator_riscv64.cpp
@@ -500,6 +500,30 @@ address TemplateInterpreterGenerator::generate_deopt_entry_for(TosState state,
   // NULL last_sp until next java call
   __ sd(zr, Address(fp, frame::interpreter_frame_last_sp_offset * wordSize));
 
+#if INCLUDE_JVMCI
+  // Check if we need to take lock at entry of synchronized method.  This can
+  // only occur on method entry so emit it only for vtos with step 0.
+  if ((EnableJVMCI || UseAOT) && state == vtos && step == 0) {
+    Label L;
+    __ lbu(t0, Address(xthread, JavaThread::pending_monitorenter_offset()));
+    __ beqz(t0, L);
+    // Clear flag.
+    __ sb(zr, Address(xthread, JavaThread::pending_monitorenter_offset()));
+    // Take lock.
+    lock_method();
+    __ bind(L);
+  } else {
+#ifdef ASSERT
+  if (EnableJVMCI) {
+    Label L;
+    lbu(t0, Address(xthread, JavaThread::pending_monitorenter_offset()));
+    beqz(t0, L);
+    __ stop("unexpected pending monitor in deopt entry");
+    __ bind(L);
+  }
+#endif
+  }
+#endif
   // handle exceptions
   {
     Label L;
